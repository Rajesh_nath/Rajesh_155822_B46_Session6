<?php
namespace App;

define("BITM","Basis Institute of Technology & Management");

echo BITM;

echo "<br>".__LINE__; //show the existing line number

echo "<br>".__FILE__;

echo "<br>".__NAMESPACE__;

class MyClass{
    public $className;
    public function myMethod(){

        $this->className = __CLASS__;
        echo __METHOD__."<br>";
    }
}

$myObject = new MyClass();
$myObject->myMethod();
echo $myObject->className."<br>";


echo "<br>".__DIR__;